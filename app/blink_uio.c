
    #include <stdio.h>
    #include <stdlib.h>
    #include <unistd.h>
    #include <sys/mman.h>
    #include <fcntl.h>
    
    #define GPIO_PHYS_ADDR 0x41200000 // GPIO connected to 8 LEDs
    
    int main (int argc , char* argv[]){
                          int   fd;
                 unsigned int   page_size = sysconf( _SC_PAGESIZE );
                 unsigned int   phys_addr, page_addr, page_offset;
                 unsigned int   initial_val;
            volatile unsigned int * gpio_ptr;
        volatile unsigned int   input_val = strtoul(argv[1], NULL, 0);
    
    // Open /dev/mem with read-write mode.
            fd = open( "/dev/uio0", O_RDWR ); // File Descriptor. fd >= 0 if successful
            if( fd < 0 ) {
                    Printf("Error opening /dev/mem\n" );
            exit(EXIT_FAILURE);
            }
    
            phys_addr   = GPIO_PHYS_ADDR;
            page_addr   = ( phys_addr & ~ ( page_size - 1 ));
            page_offset = phys_addr - page_addr;
            gpio_ptr    = mmap( NULL, page_size, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0 );
            if(gpio_ptr == MAP_FAILED) {
                    printf("Failed to map LED physical address\n" );
                    close( fd );
    return EXIT_FAILURE;
            }
            else if(gpio_ptr != MAP_FAILED){
                printf("map not failed\n");
              }
            //      gpio_ptr += page_offset;
    
            if (argc != 2 || (input_val < 0 || 255 < input_val)){
                    printf("usage: sudo ./a.out <integer>\n");
            printf("<integer> must be from 0b00000000 = 0x0 to 0b11111111 = 0x255.n");
            close(fd);
            return EXIT_FAILURE;
            }
            else{
            initial_val = * gpio_ptr;
                    * gpio_ptr = input_val;
                    printf("8-bit LEDs' value changed:  0x%d ---> 0x%d\n", initial_val, * gpio_ptr);
            }
    close(fd);
            return EXIT_SUCCESS;
    }
   
