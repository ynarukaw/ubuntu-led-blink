#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>

#define GPIO_PHYS_ADDR 0xA0010000 // GPIO connected to 8 LEDs

int main (int argc , char* argv[]){

//dev/mem(物理メモリを読み書きするデバイスファイル)を指定するためのファイルディスクリプタ。
//fdとしては0:標準入力 1:標準出力 2:標準エラー出力　は既に決まっていて3以降は任意に利用できる。

                      int   fd; 

// page_sizeは一つのページのサイズ。sysconf関数事態は動作中に設定情報を取得する関数。
             unsigned int   page_size = sysconf( _SC_PAGESIZE );

//phys_addr:物理アドレス  page_addr:ページのアドレス  page_offset:ページのオフセット
2qわさ
             unsigned int   phys_addr, page_addr, page_offset;

             unsigned int   initial_val;
	volatile unsigned int * gpio_ptr;

//input valueは標準入力値(文字列argv)をstrtoul関数で文字列をlong型に変換している。
    volatile unsigned int   input_val = strtoul(argv[1], NULL, 0);

    // Open /dev/mem with read-write mode.
	fd = open( "/dev/mem", O_RDWR ); // File Descriptor. fd >= 0 if successful
	if( fd < 0 ) {
		printf("Error opening /dev/mem\n" );
        exit(EXIT_FAILURE);
	}

	phys_addr   = GPIO_PHYS_ADDR;
	page_addr   = ( phys_addr & ~ ( page_size - 1 ));
	page_offset = phys_addr - page_addr;
	gpio_ptr    = mmap( NULL, page_size, PROT_READ|PROT_WRITE, MAP_SHARED, fd, page_addr );
	if(gpio_ptr == MAP_FAILED) {
		printf("Failed to map LED physical address\n" );
		close( fd );
		return EXIT_FAILURE;
	}
	gpio_ptr += page_offset;


	if (argc != 2 || (input_val < 0 || 255 < input_val)){
		printf("usage: sudo ./a.out <integer>\n");
        printf("<integer> must be from 0b00000000 = 0x0 to 0b11111111 = 0x255.\n");
        close(fd);
        return EXIT_FAILURE;
	}
	else{
        initial_val = * gpio_ptr;
		* gpio_ptr = input_val;
		printf("8-bit LEDs' value changed:  0x%d ---> 0x%d\n", initial_val, * gpio_ptr);
	}

    close(fd);
	return EXIT_SUCCESS;
}
