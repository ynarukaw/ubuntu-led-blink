#LED0
set_property PACKAGE_PIN AK20 [get_ports led_tri_io[0]]
set_property IOSTANDARD LVCMOS33 [get_ports led_tri_io[0]]

#LED1
set_property PACKAGE_PIN AJ19 [get_ports led_tri_io[1]]
set_property IOSTANDARD LVCMOS33 [get_ports led_tri_io[1]]

#LED2
set_property PACKAGE_PIN AK18 [get_ports led_tri_io[2]]
set_property IOSTANDARD LVCMOS33 [get_ports led_tri_io[2]]

#LED3
set_property PACKAGE_PIN AK17 [get_ports led_tri_io[3]]
set_property IOSTANDARD LVCMOS33 [get_ports led_tri_io[3]]
